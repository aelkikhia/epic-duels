package ObjectTesters;
import objects.Character;
import objects.GameTypes.*;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class CharacterTester {
    public static void main(String[] args) {
        Character char1 = new Character(PlayerType.Player1,
                                        "Count Dooku",
                                        CharType.Dooku,
                                        10,
                                        SquareState.Dark);
        System.out.println(char1);

        System.out.println();
        
        char1.setCanMove(true);
        char1.setIsMain(true);
        char1.setLife(8);
        char1.setOwner(PlayerType.none);
        char1.setType(CharType.Mace);
        char1.setName("Mace Windu");
        
        System.out.println(char1);

    }
}
