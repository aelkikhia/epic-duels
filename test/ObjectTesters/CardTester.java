package ObjectTesters;
import objects.Card;
import objects.GameTypes.*;
import initializers.CardInitializer;


/**
 *
 * @author root
 */
public class CardTester {
    public static void main(String[] args){
        Card c1 = new Card(CardName.Taunting, CardType.Special_Attack, 
                CardInitializer.DrawCards_Desc(3), CharType.Dooku, 7, 0);
        System.out.println(c1);

        System.out.println();

        c1.setAttack(0);
        c1.setDefense(0);
        c1.setDesc("Bring it!!!");
        c1.setName(CardName.Wisdom);
        c1.setOwner(CharType.Mace);

        System.out.println(c1);
    }
}
