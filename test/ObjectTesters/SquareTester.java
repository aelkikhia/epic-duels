package ObjectTesters;
import objects.GameTypes.SquareState;
import objects.Square;
import objects.Position;
/**
 *
 * @author almutaz.m.elkikhia
 */
public class SquareTester {
        public static void main(String[] args) {
            Square sq1 = new Square();
            System.out.println(sq1);

            Square sq2 = new Square(SquareState.Light, 3, 3);
            System.out.println(sq2);
            
            sq2.setPosition(new Position(5,7));
            sq2.setState(SquareState.Hole);
            System.out.println(sq2);
        }
}
