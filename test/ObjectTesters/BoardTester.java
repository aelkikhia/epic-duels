package ObjectTesters;

import objects.Board;
import objects.GameTypes.BoardType;
import initializers.BoardInitializer;

/**
 *
 * @author root
 */
public class BoardTester {
    public static void main(String[] args){
        Board b1 = new Board(BoardType.Geonosis);
        BoardInitializer.initialize(b1.getBoard(), b1.getX(), b1.getY());
        System.out.println(b1);

        b1.printBoard();
        
        BoardInitializer.buildBoard(b1);

        System.out.println(b1);

        b1.printBoard();
    }
}
