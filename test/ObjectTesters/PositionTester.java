package ObjectTesters;
import objects.Position;
/**
 *
 * @author almutaz.m.elkikhia
 */
public class PositionTester {
        public static void main(String[] args) {
            Position pos = new Position(3,5);
            System.out.println(pos);

            pos.setX(9);
            pos.setY(2);
            System.out.println(pos);

            pos = new Position(6,7);
            System.out.println(pos);
        }
}
