package Oldtesters;

import objects.Squad;
import objects.Board;
import objects.Dice;
import objects.GameTypes.BoardType;
import rules.CardRules;

/**
 *
 * @author almutaz.m.elkikhia
 */

public class ObjectTester {
    public static void main(String[] args) {
            //test code for Player, DeckInitializer, Card, CardInitializer class
            Squad t1 = new Squad();
            System.out.println("\n\n\n");
            CardRules.ShuffleDeck(t1.Deck());
            CardRules.ShuffleDeck(t1.Deck());
            CardRules.ShuffleDeck(t1.Deck());
            t1.PrintDeck();
            System.out.println("\n\n\n");
            rules.CardRules.ShuffleDeck(t1.Deck());
            t1.PrintDeck();

            //test code for board class
            Board Boardtest = new Board(BoardType.Geonosis);
            Boardtest.printBoard();
            Boardtest.printCoordinates();
            System.out.println("\n\n\n");
            Boardtest = new Board(BoardType.Throne_Room);
            Boardtest.printBoard();
            System.out.println("\n\n\n");
            Boardtest = new Board(BoardType.Freezing_Chamber);
            Boardtest.printBoard();
            System.out.println("\n\n\n");
            Boardtest = new Board(BoardType.Kamino);
            Boardtest.printBoard();



            //test code for the dice class
            Dice d1 = new Dice();
            for (int i = 0; i < 20; i++)
            {
                System.out.println("Roll = " + d1.Roll());
            }
    }

}
