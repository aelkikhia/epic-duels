package Oldtesters;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import objects.GameTypes.SquareState;
import objects.GameTypes.PlayerType;
import objects.GameTypes.CharType;
import objects.GameTypes.BoardType;
import objects.Squad;
import objects.Board;
//import rules.BoardRules;
import initializers.SquadInitializer;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class RuleTester {
    public static void main(String[] args)
        {

            Board b1 = new Board(BoardType.Geonosis);
            Squad team1 = new Squad();
            Squad team2 = new Squad();
            //BoardRules rules = new BoardRules();
            /*
            TeamInitializer.setCharacter(b1.getCharacters(), team1, PlayerType.Player1,
                CharType.Mace, SquareState.Light);
            TeamInitializer.setCharacter(b1.getCharacters(), team2, PlayerType.Player2,
                CharType.Dooku, SquareState.Dark);

            team1.PrintDeck();

            team1.PrintHand();
            team1.PrintDiscard();

            b1.printBoard();



            b1.placeCharacter(mace.Main);
            b1.placeCharacter(mace.Second_1);
            b1.placeCharacter(mace.Second_2);

            b1.placeCharacter(dooku.Main);
            b1.placeCharacter(dooku.Second_1);
            b1.placeCharacter(dooku.Second_2);

            b1.printBoard();
            

            
            System.out.println();

            //adjacency tests
            mace.Main.x = 5;
            mace.Main.y = 2;
            b1.setSquare(mace.Main);
            mace.Second_1.x = 9;
            mace.Second_1.y = 1;
            b1.setSquare(mace.Second_1);
            mace.Second_2.x = 5;
            mace.Second_2.y = 2;
            Square s1 = new Square(SquareState.Empty, 5, 2);

            dooku.Main.x = 0;
            dooku.Main.y = 0;
            b1.setSquare(dooku.Main);
            dooku.Second_1.x = 1;
            dooku.Second_1.y = 2;
            b1.setSquare(dooku.Second_1);
            dooku.Second_2.x = 2;
            dooku.Second_2.y = 5;
            b1.setSquare(dooku.Second_2);

            System.out.println(dooku.Second_1);
            System.out.println(dooku.Second_2);
            System.out.println(dooku.Main);
            System.out.println(mace.Main);
            System.out.println(mace.Second_1);
            System.out.println(mace.Second_2);
            System.out.println("\n");
            b1.printBoard();
            

            
            System.out.println("\n\nIs Adjacent");
            System.out.println(rules.isAdjacent(mace.Main, dooku.Second_1));
            System.out.println(rules.isAdjacent(mace.Main, dooku.Second_2));
            System.out.println(rules.isAdjacent(mace.Main, dooku.Main));
            System.out.println(rules.isAdjacent(mace.Main, mace.Second_1));
            System.out.println(rules.isAdjacent(mace.Main, b1.getSquare(5,1)));
            System.out.println(rules.isAdjacent(mace.Main, b1.getSquare(6,1)));
            System.out.println(rules.isAdjacent(mace.Main, b1.getSquare(7,1)));
            System.out.println(rules.isAdjacent(mace.Main, b1.getSquare(7,2)));
            System.out.println("\n\nIs Melee Legal");
            System.out.println(rules.isMeleeLegal(mace.Main, dooku.Second_1));
            System.out.println(rules.isMeleeLegal(mace.Main, dooku.Second_2));
            System.out.println(rules.isMeleeLegal(mace.Main, dooku.Main));
            System.out.println(rules.isMeleeLegal(mace.Main, mace.Second_1));
            System.out.println("\n\nIs Diagonal");
            System.out.println(rules.isDiagonal(mace.Main, dooku.Second_1));
            System.out.println(rules.isDiagonal(mace.Main, dooku.Second_2));
            System.out.println(rules.isDiagonal(mace.Main, dooku.Main));
            System.out.println(rules.isDiagonal(mace.Main, mace.Second_1));
            

            
            System.out.println("\n\nIs Parallel");
            System.out.println(rules.isParallel(mace.Main, dooku.Second_1));
            System.out.println(rules.isParallel(mace.Main, dooku.Second_2));
            System.out.println(rules.isParallel(mace.Main, dooku.Main));
            System.out.println(rules.isParallel(mace.Main, mace.Second_1));

            System.out.println("\n\nIs Parallel Path Clear");
            System.out.println(rules.isParallelClear(b1, mace.Main, dooku.Second_1));
            System.out.println(rules.isParallelClear(b1, mace.Main, dooku.Second_2));
            System.out.println(rules.isParallelClear(b1, mace.Main, dooku.Main));
            System.out.println(rules.isParallelClear(b1, mace.Main, mace.Second_1));
            


            
            System.out.println("\n\nIs Diagonal Path Clear");
            System.out.println(rules.isDiagonalClear(b1, mace.Main, dooku.Second_1));
            System.out.println(rules.isDiagonalClear(b1, mace.Main, dooku.Second_2));
            System.out.println(rules.isDiagonalClear(b1, mace.Main, dooku.Main));
            System.out.println(rules.isDiagonalClear(b1, mace.Main, mace.Second_1));
*/

            System.out.println();
        }
}
