package Oldtesters;



import objects.Board;
import objects.Squad;
import objects.GameTypes.SquareState;
import objects.GameTypes.PlayerType;
import objects.GameTypes.BoardType;
import initializers.SquadInitializer;



/**
 *
 * @author almutaz.m.elkikhia
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Board b = new Board(BoardType.Freezing_Chamber);
        Squad t1 = new Squad();
        SquadInitializer.chooseCharacter(b, t1, PlayerType.none, SquareState.Dark);
        t1.PrintDeck();
    }

}
