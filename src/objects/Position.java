/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package objects;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class Position {
    private int myX, myY;

        //constructor
        public Position(int x, int y) { myX = x; myY = y; }

	public int getX(){return myX;}
	public int getY(){return myY;}

        public void setX(int x){myX = x;}
        public void setY(int y){myY = y;}

        public boolean isEqual(Position pos){
            return getX() == pos.getX() && getY() == pos.getY();
        }

        @Override
        public String toString(){return "(" + myX + "," + myY+ ")";}
}