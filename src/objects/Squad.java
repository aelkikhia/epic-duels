package objects;

import java.util.ArrayList;
import java.util.List;
import objects.GameTypes.PlayerType;

public class Squad {
    private int myActions = 2;
    private PlayerType myOwner = PlayerType.none;
    private List<Card> myHand = new ArrayList<Card>();
    private List<Card> myDeck = new ArrayList<Card>();
    private List<Card> myDiscard = new ArrayList<Card>();
    private List<Character> myCharacters = new ArrayList<Character>();
    
    public List<Character> getSquad() { return myCharacters; }
    public void setSquad(List<Character> value){ myCharacters = value; }
    
    /// <summary>
    /// Number of Actions Remaining this round to Player
    /// </summary>
    public int getActions(){ return myActions; }
    public void setActions(int value) { myActions = value; }

    /// <summary>
    /// Get or Set Owner
    /// </summary>
    public PlayerType getOwner(){ return myOwner; }
    public void setOwner(PlayerType value) { myOwner = value; }

    /// <summary>
    /// Get or Set Player Hand
    /// </summary>
    public List<Card> Hand() { return myHand; }
    public void setHand(List<Card> value){ myHand = value; }

    /// <summary>
    /// Get or Set Deck
    /// </summary>
    public List<Card> Deck() { return myDeck; }
    public void setDeck(List<Card> value){ myDeck = value; }
    

    /// <summary>
    /// Get or Set Player Discard Pile
    /// </summary>
    public List<Card> Discard() { return myDiscard; }
    public void setDiscard(List<Card> value){ myDiscard = value; }

    /// <summary>
    /// Print Deck
    /// </summary>
    public void PrintDeck()
    {
        for (Card e : myDeck)
        {
            System.out.println(e);
        }
    }

    public void PrintHand()
    {
        for(Card e : myHand)
        {
            System.out.println(e);
        }
    }

    public void PrintDiscard()
    {
        for (Card e : myDiscard)
        {
            System.out.println(e);
        }
    }


}
