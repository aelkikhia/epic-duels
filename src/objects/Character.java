package objects;

import objects.GameTypes.CharType;
import objects.GameTypes.PlayerType;
import objects.GameTypes.SquareState;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class Character extends Square
{
        private String myName = null;
        private int myLife = 0;
        private CharType myType = CharType.none;
        private boolean isMain = false;
        private boolean myMove = false;
        private PlayerType myOwner = PlayerType.none;


        public Character(PlayerType owner, String name, CharType type, int life,
                         SquareState state)
        {
            myOwner = owner;
            myName = name;
            myType = type;
            myLife = life;
            super.myState = state;
        }

        public Character(PlayerType owner, String name, CharType type, int life,
                         boolean main, SquareState state)
        {
            myOwner = owner;
            myName = name;
            myType = type;
            myLife = life;
            isMain = main;
            super.myState = state;
        }

        public PlayerType getOwner() { return myOwner; }
        public void setOwner(PlayerType value) { myOwner = value; }

        public String getName() { return myName; }
        public void setName(String value) { myName = value; }


        public CharType getType() { return myType; }
        public void setType(CharType value) { myType = value; }


        public int getLife() { return myLife; }
        public void setLife(int value) { myLife = value; }

        public boolean isMain() { return isMain; }
        public void setIsMain(boolean value) { isMain = value; }

        public boolean canMove() { return myMove; }
        public void setCanMove(boolean value) { myMove = value; }

        @Override
        public String toString()
        {
            return "Name: " + getName()
                    + "\nCharacter Type: " + getType()
                    + "\nLife: " + getLife()
                    + "\nIs Main Character: " + isMain()
                    + "\nCan Move: " + canMove() + "\n"
                    + super.toString();
        }
}
