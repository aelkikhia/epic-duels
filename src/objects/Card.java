/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package objects;
import objects.GameTypes.CardType;
import objects.GameTypes.CardName;
import objects.GameTypes.CharType;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class Card {
    private CardName myName;
    private CardType myType;
    private String myDescription = null;
    private CharType myOwner;
    private int myAttack;
    private int myDefense;

        /// <summary>
        /// Constructor for Card class
        /// </summary>
        /// <param name="name">CardName</param>
        /// <param name="type">CardType</param>
        /// <param name="description">string</param>
        /// <param name="owner">CharType</param>
        /// <param name="attack">int</param>
        /// <param name="defense">int</param>
        public Card(CardName name, CardType type, String description, 
                CharType owner, int attack, int defense)
        {
            myName = name;
            myType = type;
            myDescription = description;
            myOwner = owner;
            myAttack = attack;
            myDefense = defense;
        }

        /// <summary>
        /// Get or Set Card Name
        /// </summary>
        public CardName getName() { return myName; }
        public void setName(CardName value) { myName = value; }

        /// <summary>
        /// get or set CardType
        /// </summary>
        public CardType getType() { return myType; }
        public void setType(CardType value) { myType = value; }

        /// <summary>
        /// get or set Card description
        /// </summary>
        public String getDesc() { return myDescription; }
        public void setDesc(String value) { myDescription = value; }

        /// <summary>
        /// get set Card Owner CharType
        /// </summary>
        public CharType getOwner() { return myOwner; }
        public void setOwner(CharType value) { myOwner = value; }

        /// <summary>
        /// Get or Set Attack value
        /// </summary>
        public int getAttack() { return myAttack; }
        public void setAttack(int value) { myAttack = value; }

        /// <summary>
        /// Get or set defense value
        /// </summary>
        public int getDefense() { return myDefense; }
        public void setDefense(int value) { myDefense = value; }

        /// <summary>
        /// to String
        /// </summary>
        /// <returns>string</returns>
        @Override
        public String toString()
        {
            return "Card Name: " + getName()
                    + "\nCard Owner: " + getOwner()
                    + "\nCard Type: " + getType()
                    + "\nCard Attack|Defense: " + getAttack()
                    + "|" + getDefense()
                    + "\nCard Desc: " + getDesc();
        }
}
