package objects;

import objects.GameTypes.BoardType;
import objects.GameTypes.SquareState;
import objects.GameTypes.CharType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class Board
    {
        private String myName = null;
        private int myX = 10;
        private int myY = 7;
        private BoardType myType;
        List<Character> charList = new ArrayList<Character>();
        private Square[][] myBoard;

        /// <summary>
        /// Board Constructor
        /// </summary>
        /// <param name="type">BoardType</param>
        public Board(BoardType type)
        {
            myType = type;
            myBoard = new Square[myX][myY];
        }

        /// <summary>
        /// Board Constructor
        /// </summary>
        /// <param name="type">BoardType</param>
        /// <param name="x">int</param>
        /// <param name="y">int</param>
        public Board(BoardType type, int x, int y)
        {
            myType = type;
            myX = x;
            myY = y;
            myBoard = new Square[x][y];
        }

        /// <summary>
        /// get/set game board
        /// </summary>
        public Square[][] getBoard() { return myBoard; }

        //set board
        public void setBoard(Square[][] b) { myBoard = b; }

        /// <summary>
        /// Get the Square at the Board[x,y]
        /// </summary>
        /// <param name="x">int</param>
        /// <param name="y">int</param>
        /// <returns>Square</returns>
        public Square getSquare(Position pos)
        {
            return myBoard[pos.getX()][pos.getY()];
        }

        /// <summary>
        /// Set the Square at the Board[x,y]
        /// </summary>
        /// <param name="s1">Square</param>
        public void setSquare(Square s1){ myBoard[s1.getX()][s1.getY()] = s1; }

        /// <summary>
        /// Return the X dimension of the Board
        /// </summary>
        public int getX() { return myX; }
        public void setX(int value){ myX = value; }

        /// <summary>
        /// Return the Y dimension of the Board
        /// </summary>
        public int getY() { return myY; }
        public void setY(int value){ myY = value; }

        public BoardType getType() { return myType; }
        public void setType(BoardType value) { myType = value; }

        /// <summary>
        /// Get/Set Board name
        /// </summary>
        public String getName() { return myName; }
        public void setName(String value) { myName = value; }

        /// <summary>
        /// Prints Coordinates of the board
        /// </summary>
        public void printCoordinates()
        {
            for (int j = 0; j < myY; j++)
            {
                for (int i = 0; i < myX; i++)
                {
                    System.out.print("[" + myBoard[i][j].getX()
                            + ", " + myBoard[i][j].getY() + "]");
                }
                System.out.println();
            }
        }

        /// <summary>
        /// Prints text-based representation of the board for testing
        /// </summary>
        public void printBoard()
        {
            System.out.println(myName);
            System.out.println("------------");
            for (int j = 0; j < myY; j++)
            {
                System.out.print("|");
                for (int i = 0; i < myX; i++)
                {
                    if (myBoard[i][j].getState() == SquareState.Obstacle)
                        System.out.print("O");
                    else if (myBoard[i][j].getState() == SquareState.Light)
                    {
                        Character temp = (Character)myBoard[i][j];
                        if (temp.getType() == CharType.Mace)
                            System.out.print("M");
                        else if (temp.getType() == CharType.Trooper)
                            System.out.print("T");
                        else
                            System.out.print("l");
                    }
                    else if (myBoard[i][j].getState() == SquareState.Dark)
                    {
                        Character temp = (Character)myBoard[i][j];
                        if (temp.getType() == CharType.Dooku)
                            System.out.print("D");
                        else if (temp.getType() == CharType.S_BattleDroid)
                            System.out.print("S");
                        else
                            System.out.print("d");
                    }
                    else if (myBoard[i][j].getState() == SquareState.Hole)
                        System.out.print("X");
                    else
                        System.out.print(" ");
                }
                System.out.println("|");
            }
            System.out.println("------------");
        }

        /// <summary>
        /// Sets Character On Board
        /// </summary>
        /// <param name="c1">Character</param>
        public void setCharacter(Character c1)
        {
            myBoard[c1.getX()][c1.getY()] = c1;
        }

    @Override
        public String toString()
        {
            return "Board Name: " + getName()
                    + "\nBoard Type: " + getType()
                    + "\nX-Dimension: " + getX()
                    + "\nY-Dimension: " + getY();
        }
    }