package objects;
import objects.GameTypes.*;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class Game {
        Dice dice = new Dice();

	Board b1;

	List<Player> players = new ArrayList<Player>();
        List<Squad> squads = new ArrayList<Squad>();

        public Game(BoardType btype){
            b1 = new Board(btype);
        }

        public List<Player> getPlayers(){ return players; }
        public void addPlayers(Player value ){ players.add(value); }

        public List<Squad> getSquads(){ return squads; }
        public void setSquads(Squad value){ squads.add(value); }

}
