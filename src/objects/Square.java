package objects;
import objects.GameTypes.SquareState;

/**
 *
 * @author almutaz.m.elkikhia
 */
   public class Square
    {
        protected Position myPos = null;
        protected SquareState myState = SquareState.Empty;

        public Square()
        {
        }

        public Square(SquareState state, int x, int y)
        {
            myState = state;
            myPos = new Position(x, y);
        }

        public SquareState getState() { return myState; }

        public void setState(SquareState value){ myState = value;}

        public Position getPosition() { return myPos; }

        public void setPosition(Position value) { myPos = value; }

        public int getX(){return myPos.getX();}
	public int getY(){return myPos.getY();}

        public void setX(int x){ myPos.setX(x); }
        public void setY(int y){ myPos.setY(y); }

        @Override
        public String toString()
        {
            return "Position: " + myPos + "\nSquare State: " + myState;
        }
    }