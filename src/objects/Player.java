package objects;

import java.util.List;
import java.util.ArrayList;
import objects.GameTypes.PlayerType;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class Player {
	private PlayerType player = null;
	private List<Squad> mySquads = new ArrayList<Squad>();
	
	public Player(PlayerType player){
		this.player = player;
	}
	
    public List<Squad> getName() { return mySquads; }
    public void setTeams(List<Squad> value) { mySquads = value; }


    public PlayerType getType() { return player; }
    public void setPlayerType(PlayerType value) { player = value; }
}
