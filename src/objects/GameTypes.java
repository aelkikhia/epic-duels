package objects;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class GameTypes {
    public enum RollType
        {
            two_all,
            three,
            three_all,
            four,
            four_all,
            five
        }
        public enum ActionType
        {
            Draw,
            Attack,
            Special,
            Special_Attack,
            Heal
        }
        public enum SquareState
        {
            Dark,
            Empty,
            Light,
            Obstacle,
            Hole
        }
        public enum BoardType
        {
            Geonosis,
            Freezing_Chamber,
            Throne_Room,
            Kamino
        }
        public enum CharType
        {
            //main characters
            Mace,
            Obi,
            Anakin,
            Luke,
            Han,
            Yoda,
            Dooku,
            Emperor,
            Vader,
            Boba,
            Jango,
            Maul,

            //special secondary
            Padme,
            Leia,
            Chewie,
            Zam,
            Greedo,

            //generic secondary
            Trooper,
            CrimsonGuard,
            BattleDroid,
            S_BattleDroid,

            none
        }
        public enum CardType
        {
            Combat,
            Special,
            Special_Attack,
            Special_Defense,
            Special_Att_Def
        }
        public enum CardName
        {
            Combat,
            MasterfulFighting,
            Battlemind,
            Wisdom,
            Whirlwind,
            GainPower,
            ForcePush,
            ForceDrain,
            GiveOrders,
            Taunting,
            DarkSideDrain,
            AllToEasy,
            Choke,
            WrathVader,
            ThrowDebris,
            YourSkillsAreNotComplete,
            ForceControl,
            JediAttack,
            JediBlock,
            JediMindTrick,
            ForceQuickness,
            ForceBalance,
            ForceStrike,
            Serenity,
            ForceRebound,
            ForceLift,
            Insight,
            AthleticSurge,
            SithSpeed,
            SuperSithSpeed,
            BlindingSurge,
            MartialDefense,
            SuddenArrival,
            DesperateShot,
            DeadlyAim,
            KyberDart,
            RocketRetreat,
            ThermalDetonator,
            WristCable,
            MissleLaunch,
            FlameThrower,
            FireUpTheJetPack,
            Assassination,
            SniperShot,
            RoyalCommand,
            ForceLightning,
            YouWillDie,
            FutureForeseen,
            LetGoOfYourHatred,
            Meditation,
            Justice,
            IWillNotFightYou,
            ChildrenOfTheForce,
            LatentForceAbilities,
            LukesInTrouble,
            HeroicRetreat,
            NeverTellMeTheOdds,
            GamblersLuck,
            Bowcaster,
            WookieHealing,
            WookieInstincts,
            ItsNotWise,
            Protection,
            PreciseShot,
            ShotOnTheRun,
            Anger,
            Calm,
            Counterattack,
            WrathAnakin
        }
        public enum PlayerType
        {
            none,
            Player1,
            Player2,
            Player3,
            Player4,
            Player5,
            Player6,
            Computer
        }

}
