/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package objects;
import objects.GameTypes.RollType;
import java.util.Random;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class Dice {
     //instantiate random class
        private static Random gen = new Random();
        //number of sides of the die
        private int sides = 0;
        public Dice()
        {
            sides = 6;
        }
        public Dice(int sides)
        {
            this.sides = sides;
        }

        public RollType Roll()
        {
            switch(gen.nextInt(sides)){
                case 0:
                    return RollType.two_all;
                case 1:
                    return RollType.three;
                case 2:
                    return RollType.three_all;
                case 3:
                    return RollType.four;
                case 4:
                    return RollType.four_all;
                case 5:
                    return RollType.five;
            }
            return null;
        }

}
