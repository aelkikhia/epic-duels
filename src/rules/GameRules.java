package rules;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class GameRules {
    
        public static boolean isGameOver() { return false; }

        public static boolean areMainsDead() { return false; }

        public static boolean areSecondsDead() { return false; }

}
