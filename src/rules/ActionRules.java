package rules;

import objects.GameTypes.ActionType;
import objects.GameTypes.CharType;
import objects.GameTypes.CardType;
import objects.Card;
import objects.Board;
import objects.Squad;
import objects.Character;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class ActionRules {
public static void chooseAction(ActionType type, Board b1)
        {

        }

        /// <summary>
        /// Verify if Player has a hand
        /// </summary>
        /// <param name="player">Player</param>
        /// <returns>bool</returns>
        public static boolean hasHand(Squad team)
        {
            return team.Hand().size() > 0;
        }

        /// <summary>
        /// Varify if Character can play specified card
        /// </summary>
        /// <param name="player">Player</param>
        /// <param name="type">CharType</param>
        /// <param name="cardNum">int</param>
        /// <returns>bool</returns>
        public static boolean canPlayCard(Squad team, CharType type, int cardNum)
        {
            if (!hasHand(team))
            {
                return false;
            }
            Card temp = (Card)team.Hand().get(cardNum);
            return type == temp.getOwner();
        }

        /// <summary>
        /// Verify if player has any actions remaining
        /// </summary>
        /// <param name="player">Player</param>
        /// <returns>bool</returns>
        public static boolean canAct(Squad team)
        {
            return team.getActions() > 0;
        }

        /// <summary>
        /// Discard a Card
        /// </summary>
        /// <param name="player">Player</param>
        /// <param name="cardNum">int</param>
        public static void Discard(Squad team, int cardNum)
        {
        	team.Discard().add((Card)team.Hand().remove(cardNum));
        }

        /// <summary>
        /// Draw a Card
        /// </summary>
        /// <param name="player">Player</param>
        public static void DrawCard(Squad team)
        {
        	team.Hand().add(team.Deck().remove(0));
        }

        /// <summary>
        /// Heal Character
        /// </summary>
        /// <param name="player">Player</param>
        /// <param name="CardNum">int</param>
        public static void DiscardHeal(Character char1, Squad team, int CardNum)
        {
            team.Discard().add(team.Hand().remove(CardNum));
        }

        public static int Attack(Board b1, Squad team1, Character char1, int cardNum1,
        		Squad team2, Character char2, int cardNum2)
        {
            Card card1 = (Card)team1.Hand().get(cardNum1);
            int damage = 0;
            if (cardNum2 >= 0)
            {
                Card card2 = (Card)team2.Hand().get(cardNum2);
                damage = card1.getAttack() - card2.getDefense();

                if (card2.getType() != CardType.Combat)
                {
                    //play out specific Card ability
                }
            }
            return damage;
        }



}
