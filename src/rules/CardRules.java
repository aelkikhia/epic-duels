/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package rules;

import java.util.Random;
import java.util.List;
import objects.Card;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class CardRules {
	
    /// <summary>
    /// Shuffles Cards
    /// </summary>
    /// <param name="deck">ArrayList</param>
    /// <returns>ArrayList</returns>
    public static void ShuffleDeck(List<Card> Deck)
    {
        Random gen = new Random();
        for (int i = 0; i < Deck.size(); i++)
        {
            // get random index
            int index = gen.nextInt(Deck.size() - 1);
            Card card = (Card)Deck.get(i);
            Deck.set(i, Deck.get(index));
            Deck.set(index, card);
        }
    }
    /*
        public static void CardActions(CardName name, Board b1, Player player1, Character char1,
                                       int CardNum1, Player player2, Character char2, int CardNum2)
        {
            switch(name)
            {
                case CardName.GiveOrders:
                    giveOrders_Act(board, player);
            }
        }
        */

        public static void giveOrders_Act()
        {

        }

        public static void Taunt_Act()
        {

        }
}
