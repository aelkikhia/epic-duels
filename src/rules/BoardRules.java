package rules;

import objects.Square;
import objects.Board;
import objects.Character;
import objects.Position;
import objects.GameTypes.SquareState;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author almutaz.m.elkikhia
 */

public class BoardRules {
/// <summary>
        /// Verify if two squares are diagonal to each other.
        /// </summary>
        /// <param name="square1">Square</param>
        /// <param name="square2">Square</param>
        /// <returns>bool</returns>
        public static boolean isDiagonal(Square square1, Square square2)
        {
            return (Math.abs(square1.getX() - square2.getX())
                 == Math.abs(square1.getY() - square2.getY()));
        }

        /// <summary>
        /// Verify if parallel to each other.
        /// </summary>
        /// <param name="square1">Square</param>
        /// <param name="square2">Square</param>
        /// <returns>bool</returns>
        public static boolean isParallel(Square square1, Square square2)
        {
            return (square1.getX() == square2.getX()
                 || square1.getY() == square2.getY());
        }

        /// <summary>
        /// Verify if the parallel path is clear between two squares.
        /// </summary>
        /// <param name="board">Board</param>
        /// <param name="square1">Square</param>
        /// <param name="square2">Square</param>
        /// <returns>bool</returns>
        public static boolean isParallelClear(Board board, Square square1,
                Square square2)
        {
            if (!isParallel(square1, square2))
            {
                return false;
            }
            if(isAdjacent(square1, square2))
            {
                return true;
            }

            int i = 0;
            int end = 0;
            int pivot = 0;

            if (square1.getX() == square2.getX())
            {
                pivot = square1.getX();
                if (square1.getY() < square2.getY())
                {
                    i = square1.getY() + 1;
                    end = square2.getY();
                }
                else
                {
                    i = square2.getY() + 1;
                    end = square1.getY();
                }

                System.out.println(board.getBoard()[pivot][i]);

                while(i != end)
                {
                    System.out.println(board.getBoard()[pivot][i]);
                    if ((board.getBoard()[pivot][i].getState()
                            == SquareState.Empty) ||
                        (board.getBoard()[pivot][i].getState()
                            == SquareState.Hole))
                    {
                        i++;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                pivot = square1.getY();
                if (square1.getX() < square2.getX())
                {
                    i = square1.getX() + 1;
                    end = square2.getX();
                }
                else
                {
                    i = square2.getX() + 1;
                    end = square1.getX();
                }

                System.out.println(board.getBoard()[i][pivot]);

                while(i != end)
                {
                    System.out.println(board.getBoard()[i][pivot]);
                    if ((board.getBoard()[i][pivot].getState() == SquareState.Empty) ||
                        (board.getBoard()[i][pivot].getState() == SquareState.Hole))
                    {
                        i++;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Verify if the diagonal path between two squares is not obstructed.
        /// </summary>
        /// <param name="board">Board</param>
        /// <param name="square1">Square</param>
        /// <param name="square2">Square</param>
        /// <returns>bool</returns>
        public static boolean isDiagonalClear(Board board, Square square1, Square square2)
        {
            if (!isDiagonal(square1, square2)) { return false; }
            if (isAdjacent(square1, square2)) { return true; }

            int x = -1;
            int y = -1;

            if (square1.getX() < square2.getX()) { x = 1; }
            if (square1.getY() < square2.getY()){ y = 1; }

            for(int i = 0; i < Math.abs(square1.getX() - square2.getX()) - 1; i++)
            {
                System.out.println(board.getBoard()[square1.getX() + x][square1.getY() + y]);
                if ((board.getBoard()[square1.getX() + x][square1.getY() + y].getState() == SquareState.Empty) ||
                    (board.getBoard()[square1.getX() + x][square1.getY() + y].getState() == SquareState.Hole))
                {
                    if (square1.getX() < square2.getX()) { x++; }
                    else { x--; }
                    if (square1.getY() < square2.getY()) { y++; }
                    else { y--; }
                }
                else { return false; }
            }
            return true;
        }

        /// <summary>
        /// Verify if target can be range attacked.
        /// </summary>
        /// <param name="board">Board</param>
        /// <param name="char1">Character</param>
        /// <param name="char2">Character</param>
        /// <returns>bool</returns>
        public static boolean isRangeLegal(Board board, Character char1,
                Character char2)
        {
            return isDiagonalClear(board, char1, char2)
                    && isLegalTarget(char1, char2);
        }

        /// <summary>
        /// Verify if target can be melee attacked.
        /// </summary>
        /// <param name="square1">Square</param>
        /// <param name="square2">Square</param>
        /// <returns>bool</returns>
        public static boolean isMeleeLegal(Character char1, Character char2)
        {
            return isLegalTarget(char1, char2) && isAdjacent(char1, char2);
        }

        /// <summary>
        /// Verify if Square is a legal target.
        /// </summary>
        /// <param name="char1">Character</param>
        /// <param name="char2">Character</param>
        /// <returns>bool</returns>
        public static boolean isLegalTarget(Character char1, Character char2)
        {
            return char1.getState() != char2.getState();
        }

        public static boolean isCharacter(Square square)
        {
            return !(square.getState() == SquareState.Hole ||
                     square.getState() == SquareState.Obstacle ||
                     square.getState() == SquareState.Empty);
        }

        /// <summary>
        /// Verify if two squares are adjacent.
        /// </summary>
        /// <param name="square1">Square</param>
        /// <param name="square2">Square</param>
        /// <returns>bool</returns>
        public static boolean isAdjacent(Square square1, Square square2)
        {
            return
                ((square1.getX() == square2.getX()
                && Math.abs(square1.getY() - square2.getY()) == 1) ||
                 (square1.getY() == square2.getY()
                 && Math.abs(square1.getX() - square2.getX()) == 1) ||
                 (isDiagonal(square1, square2)
                 && Math.abs(square1.getX() - square2.getX()) == 1));
        }

        /// <summary>
        /// get all adjacent characters to char1
        /// </summary>
        /// <param name="players">ArrayList</param>
        /// <param name="char1">Character</param>
        /// <returns>ArrayList</returns>
        public static List<Position> getAllAdjacentCharPositions(
                List<Character> characters, Character char1)
        {
            List<Position> list = new ArrayList<Position>();

            for(Character char2 : characters)
            {
                if(isAdjacent(char1, char2))
                {
                    list.add(char2.getPosition());
                }
                if (isAdjacent(char1, char2))
                {
                    list.add(char2.getPosition());
                }
                if (isAdjacent(char1, char2))
                {
                    list.add(char2.getPosition());
                }
            }

            return list;
        }

        /// <summary>
        /// Get list of all Adjacent Friendly Characters
        /// </summary>
        /// <param name="players">ArrayList</param>
        /// <param name="char1">Character</param>
        /// <returns>ArrayList</returns>
        public static List<Character> getAdjacentFriendly(
                List<Character> players, Character char1)
        {
            if (players.size() < 1)
            {
                return null;
            }

            List<Character> list = getAdjacentFriendly(players, char1);

            if (list.size() < 1)
            {
                return null;
            }

            for(Character char2 : list)
            {
                if (char1.getState() != char2.getState())
                {
                    list.add(char2);
                }
            }
            return list;
        }

        /// <summary>
        /// Get list of all Adjacent Enemy Characters
        /// </summary>
        /// <param name="players">ArrayList</param>
        /// <param name="char1">Character</param>
        /// <returns>ArrayList</returns>
        public static List<Character> getAdjacentEnemy(List<Character> players,
                Character char1)
        {
            if (players.size() < 1)
            {
                return null;
            }

            List<Character> list = getAdjacentFriendly(players, char1);

            if (list.size() < 1)
            {
                return null;
            }

            for(Character char2 : list)
            {
                if (char1.getState() != char2.getState())
                {
                    list.add(char2);
                }
            }
            return list;
        }

        /// <summary>
        /// Move Character to new Square, reset Character's previous square to empty
        /// </summary>
        /// <param name="board">Board</param>
        /// <param name="char1">Character</param>
        /// <param name="square">Square</param>
        /// <returns>Board</returns>

        public static Board moveCharacter(Board board, Character char1,
                Square square)
        {
            Square temp = new Square(SquareState.Empty, char1.getX(), char1.getY());
            char1.setX(square.getX());
            char1.setY(square.getY());
            board.setCharacter(char1);
            board.setSquare(temp);
            return board;
        }

        /*
         * function to discover all possible Positions that a character
         * can be moved to based on the number of moves and obstructions on the
         * board. Returns a List of Positions. Validation needs to be made so
         * that the Position is not taken up by a friendly character.
         */
        public static List<Position> findPossibleMoves(Board board,
                Square square, int numMoves, List<Position> listOfMoves)
        {
            if (numMoves == 0){ 
                return listOfMoves;
            }
            if (isObstructed(board, square)){ 
                return listOfMoves;
            }
            listOfMoves.add(square.getPosition());
            //decrement the number of moves if it passes all tests
            --numMoves;

            findPossibleMoves(board, new Square(square.getState(), 
                    square.getX(), square.getY() - 1), numMoves, listOfMoves);
            findPossibleMoves(board, new Square(square.getState(),
                    square.getX(), square.getY() + 1), numMoves, listOfMoves);
            findPossibleMoves(board, new Square(square.getState(),
                    square.getX() - 1, square.getY()), numMoves, listOfMoves);
            findPossibleMoves(board, new Square(square.getState(),
                    square.getX() + 1, square.getY()), numMoves, listOfMoves);
            
            return listOfMoves;
        }

        /*
         * function to check if there are obstructions with regards to
         * character movement. Empty squares and friendly characters do not
         * count as obstructions.
         */
        public static boolean isObstructed(Board board, Square square){
            if(board.getBoard()[square.getX()][square.getY() - 1].getState()
                    == SquareState.Empty
               || board.getBoard()[square.getX()][square.getY() + 1].getState()
                    == SquareState.Empty
               || board.getBoard()[square.getX() - 1][square.getY()].getState()
                    == SquareState.Empty
               || board.getBoard()[square.getX() + 1][square.getY()].getState()
                    == SquareState.Empty
               || board.getBoard()[square.getX()][square.getY() - 1].getState()
                    == square.getState()
               || board.getBoard()[square.getX()][square.getY() + 1].getState()
                    == square.getState()
               || board.getBoard()[square.getX() - 1][square.getY()].getState()
                    == square.getState()
               || board.getBoard()[square.getX() + 1][square.getY()].getState()
                    == square.getState()){
             return false;
            }
            return true;
        }
}