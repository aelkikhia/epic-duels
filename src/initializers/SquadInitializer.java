package initializers;

import java.io.InputStreamReader;
import java.io.BufferedReader;

import objects.GameTypes.PlayerType;
import objects.GameTypes.SquareState;
import objects.GameTypes.CharType;

import objects.Character;
import objects.Board;
import objects.Squad;


/**
 *
 * @author almutaz.m.elkikhia
 */
public class SquadInitializer {
/// <summary>
        /// Set Squad's character
        /// </summary>
        /// <param name="Squad"></param>
        /// <param name="type"></param>
        /// <param name="state"></param>
        public static void setCharacter(Squad squad, PlayerType owner,
                                        CharType type, SquareState state)
        {

            switch (type)
            {
                case Mace:
                    squad.getSquad().add(new Character(owner, "Mace Windu", CharType.Mace, 19, true, state));
                    squad.getSquad().add(new Character(owner, "Clone Trooper 1", CharType.Trooper, 4, state));
                    squad.getSquad().add(new Character(owner, "Clone Trooper 2", CharType.Trooper, 4, state));
                    DeckInitializer.addMaceCards(squad.Deck());
                    break;
                case Obi:
                    squad.getSquad().add(new Character(owner, "Obi-Wan Kenobi", CharType.Obi, 18, true, state));
                    squad.getSquad().add(new Character(owner, "Clone Trooper 1", CharType.Trooper, 4, state));
                    squad.getSquad().add(new Character(owner, "Clone Trooper 2", CharType.Trooper, 4, state));
                    DeckInitializer.addObiWanCards(squad.Deck());
                    break;
                case Yoda:
                    squad.getSquad().add(new Character(owner, "Yoda", CharType.Yoda, 15, true, state));
                    squad.getSquad().add(new Character(owner, "Clone Trooper 1", CharType.Trooper, 4, state));
                    squad.getSquad().add(new Character(owner, "Clone Trooper 2", CharType.Trooper, 4, state));
                    DeckInitializer.addYodaCards(squad.Deck());
                    break;
                case Anakin:
                    squad.getSquad().add(new Character(owner, "Anakin Skywalker", CharType.Anakin, 18, true, state));
                    squad.getSquad().add(new Character(owner, "Padme Amidala", CharType.Padme, 10, state));
                    DeckInitializer.addAnakinCards(squad.Deck());
                    break;
                case Luke:
                    squad.getSquad().add(new Character(owner, "Luke Skywalker", CharType.Luke, 17, true, state));
                    squad.getSquad().add(new Character(owner, "Leia Skywalker", CharType.Leia, 10, state));
                    DeckInitializer.addLukeCards(squad.Deck());
                    break;
                case Han:
                    squad.getSquad().add(new Character(owner, "Han Solo", CharType.Han, 13, true, state));
                    squad.getSquad().add(new Character(owner, "Chewbacca", CharType.Chewie, 15, state));
                    DeckInitializer.addHanCards(squad.Deck());
                    break;
                case Emperor:
                    squad.getSquad().add(new Character(owner, "Emperor Palpatine", CharType.Emperor, 13, true, state));
                    squad.getSquad().add(new Character(owner, "Crimson Guard 1", CharType.CrimsonGuard,
                                                5, state));
                    squad.getSquad().add(new Character(owner, "Crimson Guard 2", CharType.CrimsonGuard,
                                                5, state));
                    DeckInitializer.addEmperorCards(squad.Deck());
                    break;
                case Vader:
                    squad.getSquad().add(new Character(owner, "Darth Vader", CharType.Vader, 20, true, state));
                    squad.getSquad().add(new Character(owner, "Storm Trooper 1", CharType.Trooper, 4, state));
                    squad.getSquad().add(new Character(owner, "Storm Trooper 2", CharType.Trooper, 4, state));
                    DeckInitializer.addVaderCards(squad.Deck());
                    break;
                case Dooku:
                    squad.getSquad().add(new Character(owner, "Count Dooku", CharType.Dooku, 18, true, state));
                    squad.getSquad().add(new Character(owner, "Super Battle Droid 1", CharType.S_BattleDroid,
                                                5, state));
                    squad.getSquad().add(new Character(owner, "Super Battle Droid 2", CharType.S_BattleDroid,
                                                5, state));
                    DeckInitializer.addDookuCards(squad.Deck());
                    break;
                case Maul:
                    squad.getSquad().add(new Character(owner, "Darth Maul", CharType.Maul, 18, true, state));
                    squad.getSquad().add(new Character(owner, "Battle Droid 1", CharType.BattleDroid,
                                                3, state));
                    squad.getSquad().add(new Character(owner, "Battle Droid 2", CharType.BattleDroid,
                                                3, state));
                    DeckInitializer.addMaulCards(squad.Deck());
                    break;
                case Boba:
                    squad.getSquad().add(new Character(owner, "Boba Fett", CharType.Boba, 14, true, state));
                    squad.getSquad().add(new Character(owner, "Greedo", CharType.Greedo, 7, state));
                    DeckInitializer.addBobaCards(squad.Deck());
                    break;
                case Jango:
                    squad.getSquad().add(new Character(owner, "Jango Fett", CharType.Jango, 15, true, state));
                    squad.getSquad().add(new Character(owner, "Zam Wesell", CharType.Zam, 10, state));
                    DeckInitializer.addJangoCards(squad.Deck());
                    break;
                default:
                    break;
            }
        }




        public static void chooseCharacter(Board b1, Squad squad, PlayerType owner,
                                           SquareState state)
        {
            System.out.println(owner + " Choose Character:");
            System.out.println("(1)\t Mace Windu");
            System.out.println("(2)\t Obi-Wan Kenobi");
            System.out.println("(3)\t Yoda");
            System.out.println("(4)\t Anakin Skywalker");
            System.out.println("(5)\t Luke Skywalker");
            System.out.println("(6)\t Han Solo");
            System.out.println("(7)\t Emperor Palpatine");
            System.out.println("(8)\t Darth Vader");
            System.out.println("(9)\t Count Dooku");
            System.out.println("(10)\tDarth Maul");
            System.out.println("(11)\tBoba Fett");
            System.out.println("(12)\tJango Fett");

            InputStreamReader converter = new InputStreamReader(System.in);
            BufferedReader in = new BufferedReader(converter);
            String choice = "";
            try{
                choice = in.readLine();
            }
            catch(Exception e){
                System.out.println("You're retarded");
                System.exit(0);
            }


            System.out.println();

            switch (Integer.parseInt(choice))
            {
                case 1:
                    squad.getSquad().add(new Character(owner, "Mace Windu", CharType.Mace, 19, true,
                                           state));
                    squad.getSquad().add(new Character(owner, "Clone Trooper 1", CharType.Trooper, 4,
                                           state));
                    squad.getSquad().add(new Character(owner, "Clone Trooper 2", CharType.Trooper, 4,
                                           state));
                    DeckInitializer.addMaceCards(squad.Deck());
                    break;
                case 2:
                    squad.getSquad().add(new Character(owner, "Obi-Wan Kenobi", CharType.Obi, 18, true,
                                           state));
                    squad.getSquad().add(new Character(owner, "Clone Trooper 1", CharType.Trooper, 4,
                                           state));
                    squad.getSquad().add(new Character(owner, "Clone Trooper 2", CharType.Trooper, 4,
                                           state));
                    DeckInitializer.addObiWanCards(squad.Deck());
                    break;
                case 3:
                    squad.getSquad().add(new Character(owner, "Yoda", CharType.Yoda, 15, true, state));
                    squad.getSquad().add(new Character(owner, "Clone Trooper 1", CharType.Trooper, 4,
                                           state));
                    squad.getSquad().add(new Character(owner, "Clone Trooper 2", CharType.Trooper, 4,
                                           state));
                    DeckInitializer.addYodaCards(squad.Deck());
                    break;
                case 4:
                    squad.getSquad().add(new Character(owner, "Anakin Skywalker", CharType.Anakin, 18,
                                           true, state));
                    squad.getSquad().add(new Character(owner, "Padme Amidala", CharType.Padme, 10,
                                           state));
                    DeckInitializer.addAnakinCards(squad.Deck());
                    break;
                case 5:
                    squad.getSquad().add(new Character(owner, "Luke Skywalker", CharType.Luke, 17, true,
                                           state));
                    squad.getSquad().add(new Character(owner, "Leia Skywalker", CharType.Leia, 10,
                                           state));
                    DeckInitializer.addLukeCards(squad.Deck());
                    break;
                case 6:
                    squad.getSquad().add(new Character(owner, "Han Solo", CharType.Han, 13, true,
                                           state));
                    squad.getSquad().add(new Character(owner, "Chewbacca", CharType.Chewie, 15, state));
                    DeckInitializer.addHanCards(squad.Deck());
                    break;
                case 7:
                    squad.getSquad().add(new Character(owner, "Emperor Palpatine", CharType.Emperor, 13,
                                           true, state));
                    squad.getSquad().add(new Character(owner, "Crimson Guard 1", CharType.CrimsonGuard,
                                           5, state));
                    squad.getSquad().add(new Character(owner, "Crimson Guard 2", CharType.CrimsonGuard,
                                           5, state));
                    DeckInitializer.addEmperorCards(squad.Deck());
                    break;
                case 8:
                    squad.getSquad().add(new Character(owner, "Darth Vader", CharType.Vader, 20, true,
                                           state));
                    squad.getSquad().add(new Character(owner, "Storm Trooper 1", CharType.Trooper, 4,
                                           state));
                    squad.getSquad().add(new Character(owner, "Storm Trooper 2", CharType.Trooper, 4,
                                           state));
                    DeckInitializer.addVaderCards(squad.Deck());
                    break;
                case 9:
                    squad.getSquad().add(new Character(owner, "Count Dooku", CharType.Dooku, 18, true,
                                           state));
                    squad.getSquad().add(new Character(owner, "Super Battle Droid 1",
                                           CharType.S_BattleDroid, 5, state));
                    squad.getSquad().add(new Character(owner, "Super Battle Droid 2",
                                           CharType.S_BattleDroid, 5, state));
                    DeckInitializer.addDookuCards(squad.Deck());
                    break;
                case 10:
                    squad.getSquad().add(new Character(owner, "Darth Maul", CharType.Maul, 18, true,
                                           state));
                    squad.getSquad().add(new Character(owner, "Battle Droid 1", CharType.BattleDroid,
                                           3, state));
                    squad.getSquad().add(new Character(owner, "Battle Droid 2", CharType.BattleDroid,
                                           3, state));
                    DeckInitializer.addMaulCards(squad.Deck());
                    break;
                case 11:
                    squad.getSquad().add(new Character(owner, "Boba Fett", CharType.Boba, 14, true,
                                           state));
                    squad.getSquad().add(new Character(owner, "Greedo", CharType.Greedo, 7, state));
                    DeckInitializer.addBobaCards(squad.Deck());
                    break;
                case 12:
                    squad.getSquad().add(new Character(owner, "Jango Fett", CharType.Jango, 15, true,
                                           state));
                    squad.getSquad().add(new Character(owner, "Zam Wesell", CharType.Zam, 10,
                                           state));
                    DeckInitializer.addJangoCards(squad.Deck());
                    break;
            }
        }

}
