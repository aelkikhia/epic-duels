/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package initializers;
import objects.Card;
import objects.GameTypes.CharType;
import objects.GameTypes.CardName;
import objects.GameTypes.CardType;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class CardInitializer {
//Special Cards
        public static Card Whirlwind(CharType type)
        {
            Card temp = new Card(CardName.Whirlwind, CardType.Special,
                                 Whirlwind_Desc(), type, 0, 0);
            return temp;
        }

        public static Card Wisdom(CharType type)
        {
            Card temp = new Card(CardName.Wisdom, CardType.Special,
                                 Wisdom_Desc(), type, 0, 0);
            return temp;
        }

        public static Card GainPower(CharType type)
        {
            Card temp = new Card(CardName.GainPower, CardType.Special,
                                 DrawCards_Desc(3), type, 0, 0);
            return temp;
        }

        public static Card GiveOrders(CharType type)
        {
            Card temp = new Card(CardName.GiveOrders, CardType.Special,
                                 GiveOrders_Desc(), type, 0, 0);
            return temp;
        }

        public static Card ForcePush(CharType type, int damage)
        {
            Card temp = new Card(CardName.ForcePush, CardType.Special,
                                 ForcePush_Desc(type, damage), type, 0, 0);
            return temp;
        }

        public static Card ForceDrain(CharType type)
        {
            Card temp = new Card(CardName.ForceDrain, CardType.Special,
                                 ForceDrain_Desc(), type, 0, 0);
            return temp;
        }

        public static Card Choke(CharType type)
        {
            Card temp = new Card(CardName.Choke, CardType.Special,
                                 Choke_Desc(), type, 0, 0);
            return temp;
        }

        public static Card WrathVader(CharType type)
        {
            Card temp = new Card(CardName.WrathVader, CardType.Special,
                                 WrathVader_Desc(), type, 0, 0);
            return temp;
        }

        public static Card ThrowDebris(CharType type)
        {
            Card temp = new Card(CardName.ThrowDebris, CardType.Special,
                                 ThrowDebris_Desc(), type, 0, 0);
            return temp;
        }

        public static Card SkillsNotComplete(CharType type)
        {
            Card temp = new Card(CardName.YourSkillsAreNotComplete, CardType.Special,
                                 SkillsNotComplete_Desc(), type, 0, 0);
            return temp;
        }

        public static Card JediMindTrick(CharType type)
        {
            Card temp = new Card(CardName.JediMindTrick, CardType.Special,
                                 JediMindTrick_Desc(), type, 0, 0);
            return temp;
        }

        public static Card ForceQuickness(CharType type)
        {
            Card temp = new Card(CardName.ForceQuickness, CardType.Special,
                                 ForceQuickness_Desc(), type, 0, 0);
            return temp;
        }

        public static Card ForceBalance(CharType type)
        {
            Card temp = new Card(CardName.ForceBalance, CardType.Special,
                                 ForceBalance_Desc(), type, 0, 0);
            return temp;
        }

        public static Card ForceLift(CharType type)
        {
            Card temp = new Card(CardName.ForceLift, CardType.Special,
                                 ForceLift_Desc(), type, 0, 0);
            return temp;
        }

        public static Card Insight(CharType type)
        {
            Card temp = new Card(CardName.Insight, CardType.Special,
                                 Insight_Desc(), type, 0, 0);
            return temp;
        }

        public static Card ThermalDetonator(CharType type)
        {
            Card temp = new Card(CardName.ThermalDetonator, CardType.Special,
                                 ThermalDetonator_Desc(), type, 0, 0);
            return temp;
        }

        public static Card WristCable(CharType type)
        {
            Card temp = new Card(CardName.WristCable, CardType.Special,
                                 WristCable_Desc(type), type, 0, 0);
            return temp;
        }

        public static Card SuddenArrival(CharType type)
        {
            Card temp = new Card(CardName.SuddenArrival, CardType.Special,
                                 SuddenArrival_Desc(), type, 0, 0);
            return temp;
        }

        public static Card FireUpTheJetPack(CharType type)
        {
            Card temp = new Card(CardName.FireUpTheJetPack, CardType.Special,
                                 FireUpTheJetPack_Desc(), type, 0, 0);
            return temp;
        }

        public static Card FlameThrower(CharType type)
        {
            Card temp = new Card(CardName.FlameThrower, CardType.Special,
                                 FlameThrower_Desc(), type, 0, 0);
            return temp;
        }

        public static Card RoyalCommand(CharType type)
        {
            Card temp = new Card(CardName.RoyalCommand, CardType.Special,
                                 RoyalCommand_Desc(), type, 0, 0);
            return temp;
        }

        public static Card YouWillDie(CharType type)
        {
            Card temp = new Card(CardName.YouWillDie, CardType.Special,
                                 YouWillDie_Desc(), type, 0, 0);
            return temp;
        }

        public static Card FutureForeseen(CharType type)
        {
            Card temp = new Card(CardName.FutureForeseen, CardType.Special,
                                 FutureForeseen_Desc(), type, 0, 0);
            return temp;
        }

        public static Card LetGoOfYourHatred(CharType type)
        {
            Card temp = new Card(CardName.LetGoOfYourHatred, CardType.Special,
                                 LetGoOfYourHatred_Desc(), type, 0, 0);
            return temp;
        }

        public static Card ForceLightning(CharType type)
        {
            Card temp = new Card(CardName.ForceLightning, CardType.Special,
                                 ForceLightning_Desc(), type, 0, 0);
            return temp;
        }

        public static Card Meditation(CharType type)
        {
            Card temp = new Card(CardName.Meditation, CardType.Special,
                                 Meditation_Desc(), type, 0, 0);
            return temp;
        }

        public static Card WookieHealing(CharType type)
        {
            Card temp = new Card(CardName.WookieHealing, CardType.Special,
                                 WookieHealing_Desc(), type, 0, 0);
            return temp;
        }

        public static Card WookieInstincts(CharType type)
        {
            Card temp = new Card(CardName.WookieInstincts, CardType.Special,
                                 WookieInstincts_Desc(), type, 0, 0);
            return temp;
        }

        public static Card ItsNotWise(CharType type)
        {
            Card temp = new Card(CardName.ItsNotWise, CardType.Special,
                                 ItsNotWise_Desc(), type, 0, 0);
            return temp;
        }

        public static Card NeverTellMeTheOdds(CharType type)
        {
            Card temp = new Card(CardName.NeverTellMeTheOdds, CardType.Special,
                                 NeverTellMeTheOdds_Desc(), type, 0, 0);
            return temp;
        }

        public static Card WrathAnakin(CharType type)
        {
            Card temp = new Card(CardName.WrathAnakin, CardType.Special,
                                 WrathAnakin_Desc(), type, 0, 0);
            return temp;
        }

        public static Card Calm(CharType type)
        {
            Card temp = new Card(CardName.Calm, CardType.Special, Calm_Desc(), type, 0, 0);
            return temp;
        }

        public static Card Protection(CharType type)
        {
            Card temp = new Card(CardName.Protection, CardType.Special,
                                 Protection_Desc(), type, 0, 0);
            return temp;
        }

        public static Card ChildrenOfTheForce(CharType type)
        {
            Card temp = new Card(CardName.ChildrenOfTheForce, CardType.Special,
                                 ChildrenOfTheForce_Desc(), type, 0, 0);
            return temp;
        }

        public static Card IWillNotFightYou(CharType type)
        {
            Card temp = new Card(CardName.IWillNotFightYou, CardType.Special,
                                 IWillNotFightYou_Desc(), type, 0, 0);
            return temp;
        }

        public static Card LukesInTrouble(CharType type)
        {
            Card temp = new Card(CardName.LukesInTrouble, CardType.Special,
                                 LukesInTrouble_Desc(), type, 0, 0);
            return temp;
        }


        //Special Attack/Defense Cards
        public static Card MasterfulFighting(CharType type)
        {
            Card temp = new Card(CardName.MasterfulFighting, CardType.Special_Attack,
                                 DrawCards_Desc(1), type, 5, 0);
            return temp;
        }

        public static Card BattleMind(CharType type)
        {
            Card temp = new Card(CardName.Battlemind, CardType.Special_Att_Def,
                                 BattleMind_Desc(), type, 0, 0);
            return temp;
        }

        public static Card Taunting(CharType type)
        {
            Card temp = new Card(CardName.Taunting, CardType.Special_Attack,
                                 DrawCards_Desc(1), type, 7, 0);
            return temp;
        }

        public static Card DarkSideDrain(CharType type)
        {
            Card temp = new Card(CardName.DarkSideDrain, CardType.Special_Attack,
                                 DarkSideDrain_Desc(), type, 3, 0);
            return temp;
        }

        public static Card AllToEasy(CharType type)
        {
            Card temp = new Card(CardName.AllToEasy, CardType.Special_Attack,
                                 AllTooEasy_Desc(), type, 3, 0);
            return temp;
        }

        public static Card ForceControl(CharType type)
        {
            Card temp = new Card(CardName.ForceControl, CardType.Special_Attack,
                                 ForceControl_Desc(), type, 7, 0);
            return temp;
        }

        public static Card JediAttack(CharType type)
        {
            Card temp = new Card(CardName.JediAttack, CardType.Special_Attack,
                                 AttackAndMove_Desc(type, 6), type, 6, 0);
            return temp;
        }

        public static Card JediBlock(CharType type)
        {
            Card temp = new Card(CardName.JediBlock, CardType.Special_Defense,
                                 DrawCards_Desc(1), type, 0, 12);
            return temp;
        }

        public static Card ForceRebound(CharType type)
        {
            Card temp = new Card(CardName.ForceRebound, CardType.Special_Defense,
                                 ForceRebound_Desc(), type, 0, 0);
            return temp;
        }

        public static Card Serenity(CharType type)
        {
            Card temp = new Card(CardName.Serenity, CardType.Special_Defense,
                                 DrawCards_Desc(1), type, 0, 15);
            return temp;
        }

        public static Card ForceStrike(CharType type)
        {
            Card temp = new Card(CardName.ForceStrike, CardType.Special_Attack,
                                 DrawCards_Desc(1), type, 6, 0);
            return temp;
        }

        public static Card AthleticSurge(CharType type)
        {
            Card temp = new Card(CardName.AthleticSurge, CardType.Special_Attack,
                                 AttackAndMove_Desc(type, 6), type, 6, 0);
            return temp;
        }

        public static Card SithSpeed(CharType type)
        {
            Card temp = new Card(CardName.SithSpeed, CardType.Special_Attack,
                                 NotAction_Desc(), type, 3, 0);
            return temp;
        }

        public static Card SuperSithSpeed(CharType type)
        {
            Card temp = new Card(CardName.SuperSithSpeed, CardType.Special_Attack,
                                 NotAction_Desc(), type, 4, 0);
            return temp;
        }

        public static Card BlindingSurge(CharType type)
        {
            Card temp = new Card(CardName.BlindingSurge, CardType.Special_Defense,
                                 BlindingSurge_Desc(), type, 0, 0);
            return temp;
        }

        public static Card MartialDefense(CharType type)
        {
            Card temp = new Card(CardName.MartialDefense, CardType.Special_Defense,
                                 DrawCards_Desc(1), type, 0, 10);
            return temp;
        }

        public static Card KyberDart(CharType type)
        {
            Card temp = new Card(CardName.KyberDart, CardType.Special_Attack,
                                 KyberDart_Desc(), type, 9, 0);
            return temp;
        }

        public static Card DeadlyAim(CharType type)
        {
            Card temp = new Card(CardName.DeadlyAim, CardType.Special_Attack,
                                 DrawCards_Desc(2), type, 7, 0);
            return temp;
        }

        public static Card RocketRetreat(CharType type)
        {
            Card temp = new Card(CardName.RocketRetreat, CardType.Special_Attack,
                                 RocketRetreat_Desc(type), type, 4, 0);
            return temp;
        }

        public static Card DesperateShot(CharType type)
        {
            Card temp = new Card(CardName.DesperateShot, CardType.Special_Attack,
                                 DesperateShot_Desc(), type, 7, 0);
            return temp;
        }

        public static Card SniperShot(CharType type)
        {
            Card temp = new Card(CardName.SniperShot, CardType.Special_Attack,
                                 SniperShot_Desc(), type, 3, 0);
            return temp;
        }

        public static Card Assassination(CharType type)
        {
            Card temp = new Card(CardName.Assassination, CardType.Special_Attack,
                                 Assassination_Desc(), type, 7, 0);
            return temp;
        }

        public static Card MissileLaunch(CharType type)
        {
            Card temp = new Card(CardName.MissleLaunch, CardType.Special_Attack,
                                 DrawCards_Desc(4), type, 7, 0);
            return temp;
        }

        public static Card Justice(CharType type)
        {
            Card temp = new Card(CardName.Justice, CardType.Special_Attack,
                                 Justice_Desc(), type, 4, 0);
            return temp;
        }

        public static Card LatentForceAbilities(CharType type)
        {
            Card temp = new Card(CardName.LatentForceAbilities, CardType.Special_Att_Def,
                                 DrawCards_Desc(1), type, 7, 7);
            return temp;
        }

        public static Card Counterattack(CharType type)
        {
            Card temp = new Card(CardName.Counterattack, CardType.Special_Defense,
                                 Counterattack_Desc(), type, 0, 0);
            return temp;
        }

        public static Card Anger(CharType type)
        {
            Card temp = new Card(CardName.Anger, CardType.Special_Attack,
                                 Anger_Desc(), type, 8, 0);
            return temp;
        }

        public static Card ShotOnTheRun(CharType type)
        {
            Card temp = new Card(CardName.ShotOnTheRun, CardType.Special_Attack,
                                 ShotOnTheRun_Desc(), type, 6, 0);
            return temp;
        }

        public static Card PreciseShot(CharType type)
        {
            Card temp = new Card(CardName.PreciseShot, CardType.Special_Attack,
                                 PreciseShot_Desc(), type, 9, 0);
            return temp;
        }

        public static Card Bowcaster(CharType type)
        {
            Card temp = new Card(CardName.Bowcaster, CardType.Special_Attack,
                                 DrawCards_Desc(1), type, 11, 0);
            return temp;
        }

        public static Card GamblersLuck(CharType type)
        {
            Card temp = new Card(CardName.GamblersLuck, CardType.Special_Attack,
                                 GamblersLuck_Desc(), type, 4, 0);
            return temp;
        }

        public static Card HeroicRetreat(CharType type)
        {
            Card temp = new Card(CardName.HeroicRetreat, CardType.Special_Attack,
                                 HeroicRetreat_Desc(), type, 5, 0);
            return temp;
        }


        //Attack Defense Cards
        public static Card Card_5_1(CharType type)
        {
            Card temp = new Card(CardName.Combat, CardType.Combat,
                              null, type, 5, 1);
            return temp;
        }

        public static Card Card_4_2(CharType type)
        {
            Card temp = new Card(CardName.Combat, CardType.Combat,
                              null, type, 4, 2);
            return temp;
        }

        public static Card Card_4_1(CharType type)
        {
            Card temp = new Card(CardName.Combat, CardType.Combat,
                              null, type, 4, 1);
            return temp;
        }

        public static Card Card_3_3(CharType type)
        {
            Card temp = new Card(CardName.Combat, CardType.Combat,
                              null, type, 3, 3);
            return temp;
        }

        public static Card Card_3_2(CharType type)
        {
            Card temp = new Card(CardName.Combat, CardType.Combat,
                              null, type, 3, 2);
            return temp;
        }

        public static Card Card_2_3(CharType type)
        {
            Card temp = new Card(CardName.Combat, CardType.Combat,
                              null, type, 2, 3);
            return temp;
        }

        public static Card Card_2_4(CharType type)
        {
            Card temp = new Card(CardName.Combat, CardType.Combat,
                              null, type, 2, 4);
            return temp;
        }

        public static Card Card_1_4(CharType type)
        {
            Card temp = new Card(CardName.Combat, CardType.Combat,
                              null, type, 1, 4);
            return temp;
        }

        public static Card Card_1_5(CharType type)
        {
            Card temp = new Card(CardName.Combat, CardType.Combat,
                              null, type, 1, 5);
            return temp;
        }

        public static Card Card_3_1(CharType type)
        {
            Card temp = new Card(CardName.Combat, CardType.Combat,
                              null, type, 3, 1);
            return temp;
        }

        public static Card Card_2_2(CharType type)
        {
            Card temp = new Card(CardName.Combat, CardType.Combat,
                              null, type, 2, 2);
            return temp;
        }

        public static Card Card_2_1(CharType type)
        {
            Card temp = new Card(CardName.Combat, CardType.Combat,
                              null, type, 2, 1);
            return temp;
        }

        public static Card Card_1_2(CharType type)
        {
            Card temp = new Card(CardName.Combat, CardType.Combat,
                              null, type, 1, 2);
            return temp;
        }


        /*
         *
         * CARD DESCRIPTIONS
         *
         *
         */

        public static String BattleMind_Desc()
        {
            return "The attack and defense values of this card are equal to " +
                    "the number of cards in your hand after this card is played.";
        }

        public static String DrawCards_Desc(int num)
        {
            if (num == 1)
            {
                return "Draw a Card.";
            }
            else
            {
                return "Draw " + num + " Cards.";
            }
        }

        public static String Wisdom_Desc()
        {
            return "You may move Mace up to 5 Spaces, then draw a card.";
        }

        public static String Whirlwind_Desc()
        {
            return "Mace does 4 damage to all opponents' characters he can attack.";
        }

        public static String GiveOrders_Desc()
        {
            return "Move Dooku up to 4 spaces. Then move Super Battledroid 1 up to " +
                "4 spaces and move Super Battledroid 2 up to 4 spaces.";
        }

        public static String ForcePush_Desc(CharType type, int damage)
        {
            return "Move any character adjacent to" + type + "to any empty space." +
                " That character receives " + damage + " damage.";
        }

        public static String ForceDrain_Desc()
        {
            return "Pick 2 cards at random from an opponent's hand. That player must "
                + "discard those cards.";
        }

        public static String DarkSideDrain_Desc()
        {
            return "If Vader does damage to a character with this card, Vader recovers "
                + "the amount of damage done to the character.";
        }

        public static String AllTooEasy_Desc()
        {
            return "If this card is not blocked, the attacked character receives 20 damage "
                + "instead of 3.";
        }

        public static String Choke_Desc()
        {
            return "Choose any minor character. That character receives 6 damage.";
        }

        public static String WrathVader_Desc()
        {
            return "Choose an opponent. All of that opponent's characters receive 2 damage.";
        }

        public static String ThrowDebris_Desc()
        {
            return "Choose any character. That character receives 4 damage.";
        }

        public static String SkillsNotComplete_Desc()
        {
            return "Choose any opponent. That opponent must reveal his/her hand and discard "
                + "all special cards.";
        }

        public static String ForceControl_Desc()
        {
            return "After attacking, you may move all characters in play up to 3 spaces each.";
        }

        public static String AttackAndMove_Desc(CharType character, int moves)
        {
            String name = "Obi-Wan";
            if(CharType.Maul == character)
            {
                name = "Darth Maul";
            }
            return "After attacking, you may move " + name + " up to " + moves + " spaces.";
        }

        public static String JediMindTrick_Desc()
        {
                return "Take any card from your discard pile and put that card in your hand.";
        }

        public static String ForceQuickness_Desc()
        {
            return "Move Obi-Wan up to 8 spaces, then draw a card";
        }

        public static String ForceBalance_Desc()
        {
            return "All players discard their hands. Each player draws 3 new cards.";
        }

        public static String ForceLift_Desc()
        {
            return "Turn any character adjacent to Yoda on its side. This character "
                + "cannot move, attack, or defend. At any time, any player may discard "
                + "3 cards to stand this character up.";
        }

        public static String Insight_Desc()
        {
            return "Look at any opponent's hand. Then chooose one card. Your opponent "
                + "must discard the chosen card.";
        }

        public static String ForceRebound_Desc()
        {
            return "Yoda receives no damage from the attack. Instead, the attacker receives "
                + "damage equal to the attack number on the attacker's card.";
        }

        public static String NotAction_Desc()
        {
            return "Playing this card does not count as an action.";
        }

        public static String BlindingSurge_Desc()
        {
            return "After taking the attacker's damage, Darth Maul does 3 points of damage "
                + "to the attacking character.";
        }

        public static String DesperateShot_Desc()
        {
            return "If you do not destroy the defending character with this card, destroy "
                + "Greedo.";
        }

        public static String SuddenArrival_Desc()
        {
            return "Move Greedo adjacent to any character. " + NotAction_Desc();
        }

        public static String WristCable_Desc(CharType type)
        {
            return "Wrist Cable does 2 damage to any one character " + type + " Fett can "
                + "attack. The player controlling the attacked character gets 1 less action "
                + "on his/her next turn";
        }

        public static String RocketRetreat_Desc(CharType type)
        {
            return "After attacking, you may move " + type + " Fett to an empty space.";
        }

        public static String ThermalDetonator_Desc()
        {
            return "Thermal Detonator does 4 damage to any one character Boba Fett can "
                + "attack. All character adjacent to that character also  receive 4 damage.";
        }

        public static String KyberDart_Desc()
        {
            return "After attacking, if you destroy the defending character, "
                + DrawCards_Desc(3);
        }

        public static String FlameThrower_Desc()
        {
            return "Flame Thrower does 2 damage to all characters adjacent to Jango Fett. "
                + "you may then move these characters up to 3 spaces each.";
        }

        public static String FireUpTheJetPack_Desc()
        {
            return "You may move Jango Fett to any empty space. " + NotAction_Desc();
        }

        public static String Assassination_Desc()
        {
            return "After attacking, you may move Zam to an empty space.";
        }

        public static String SniperShot_Desc()
        {
            return "If this card is not blocked, Zam does 6 damage instead of 3.";
        }

        public static String Meditation_Desc()
        {
            return "The Emperor recovers up to 4 damage. Choose an opponent. That opponent "
                + "cannot draw cards during his/her next turn.";
        }

        public static String ForceLightning_Desc()
        {
            return "Choose any character. That character receives 3 damage. The player "
                + "controlling this character must discard a card at random.";
        }

        public static String LetGoOfYourHatred_Desc()
        {
            return "Choose an opponent. That opponent chooses and discards 2 cards.";
        }

        public static String FutureForeseen_Desc()
        {
            return "Look at the top 4 cards of your draw pile. Put one in your hand and "
                + "put the other 3 cards back on top of your draw pile in any order.";
        }

        public static String YouWillDie_Desc()
        {
            return "Choose an opponent. That opponent must discard his/her entire hand.";
        }

        public static String RoyalCommand_Desc()
        {
            return "Exchange spaces between the Emperor and any Crimson Guard.";
        }

        public static String Justice_Desc()
        {
            return "If Leia has been destroyed, the attack value of this card is 10.";
        }

        public static String IWillNotFightYou_Desc()
        {
            return "Choose an opponent. You and the chosen opponent reveal your hands. "
                + "Both of you discard all cards with an attack value greater than 1.";
        }

        public static String ChildrenOfTheForce_Desc()
        {
            return "Move Luke up to 6 spaces. Then move Leia up to 6 spaces. "
                + DrawCards_Desc(2);
        }

        public static String LukesInTrouble_Desc()
        {
            return "If Leia is adjacent to Luke, Luke recovers 3 damage. If Luke has been "
                + "destroyed, Leia recovers 3 damage.";
        }

        public static String Protection_Desc()
        {
            return "If Anakin is alive, Padme recovers 4 damage. If Anakin has been "
                + "destroyed, Padme recovers 2 damage.";
        }

        public static String PreciseShot_Desc()
        {
            return "After attacking, you may discard a card to draw a card.";
        }

        public static String ShotOnTheRun_Desc()
        {
            return "After attacking, you may move Padme up to 6 spaces.";
        }

        public static String Calm_Desc()
        {
            return "Move Anakin up to 8 spaces. If, after playing this card you have no "
                + "cards in your hand, draw up to 5 cards.";
        }

        public static String Anger_Desc()
        {
            return "After attacking, discard every card in your hand, except one card.";
        }

        public static String Counterattack_Desc()
        {
            return "Anakin receives no damage from the attack. Instead the attacker "
                + "receives 1 damage.";
        }

        public static String WrathAnakin_Desc()
        {
            return "You may move Anakin adjacent to any minor Character. That character "
                + "receives 7 damage.";
        }

        public static String ItsNotWise_Desc()
        {
            return "Move any character adjacent to Chewbacca up to 3 spaces. That "
                + "character receives 3 damage.";
        }

        public static String WookieInstincts_Desc()
        {
            return "Search your draw pile for the Bowcaster Attack card. If it is in your "
            + "draw pile, put it in your hand. Then shuffle your draw pile.";
        }

        public static String WookieHealing_Desc()
        {
            return "Chewbacca recovers up to 3 damage. The you may move Chewbacca up to "
                + "5 spaces.";
        }

        public static String HeroicRetreat_Desc()
        {
            return "After attacking, you may move Han up to 5 spaces.";
        }

        public static String GamblersLuck_Desc()
        {
            return "If Han does damage with this card, choose an opponent to discard "
                + "a card at random.";
        }

        public static String NeverTellMeTheOdds_Desc()
        {
            return "Han does 2 damage to all opponents' characters Han can attack. "
                + "Then you may shuffle your discard pile into your draw pile.";
        }

}
