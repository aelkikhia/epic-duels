package initializers;
import objects.GameTypes.CharType;
import objects.Card;
import java.util.List;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class DeckInitializer {
        /// <summary>
        /// Populates Mace Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        public static List<Card> addMaceCards(List<Card> deck)
        {
            CharType type = CharType.Mace;
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_3(type));
            deck.add(CardInitializer.Card_3_3(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.MasterfulFighting(type));
            deck.add(CardInitializer.MasterfulFighting(type));
            deck.add(CardInitializer.BattleMind(type));
            deck.add(CardInitializer.BattleMind(type));
            deck.add(CardInitializer.BattleMind(type));
            deck.add(CardInitializer.BattleMind(type));
            deck.add(CardInitializer.Whirlwind(type));
            deck.add(CardInitializer.Whirlwind(type));
            deck.add(CardInitializer.Wisdom(type));
            deck.add(CardInitializer.Wisdom(type));
            deck.add(CardInitializer.Wisdom(type));
            deck.add(CardInitializer.Wisdom(type));

            //Secondary Characters Cards
            addTrooperCards(deck, CharType.Trooper);
            return deck;
        }

        /// <summary>
        /// Populates Dooku Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        public static List<Card> addDookuCards(List<Card> deck)
        {
            CharType type = CharType.Dooku;
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_3(type));
            deck.add(CardInitializer.Card_3_3(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Taunting(type));
            deck.add(CardInitializer.Taunting(type));
            deck.add(CardInitializer.Taunting(type));
            deck.add(CardInitializer.Taunting(type));
            deck.add(CardInitializer.ForcePush(type, 1));
            deck.add(CardInitializer.ForcePush(type, 1));
            deck.add(CardInitializer.GainPower(type));
            deck.add(CardInitializer.GainPower(type));
            deck.add(CardInitializer.ForceDrain(type));
            deck.add(CardInitializer.GiveOrders(type));
            deck.add(CardInitializer.GiveOrders(type));
            deck.add(CardInitializer.GiveOrders(type));

            //Secondary Characters Cards
            addSuperBattleDroidCards(deck, CharType.S_BattleDroid);
            return deck;
        }

        /// <summary>
        /// Populates Vader Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        public static List<Card> addVaderCards(List<Card> deck)
        {
            CharType type = CharType.Vader;
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_2(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.DarkSideDrain(type));
            deck.add(CardInitializer.DarkSideDrain(type));
            deck.add(CardInitializer.AllToEasy(type));
            deck.add(CardInitializer.WrathVader(type));
            deck.add(CardInitializer.WrathVader(type));
            deck.add(CardInitializer.WrathVader(type));
            deck.add(CardInitializer.Choke(type));
            deck.add(CardInitializer.Choke(type));
            deck.add(CardInitializer.Choke(type));
            deck.add(CardInitializer.ThrowDebris(type));
            deck.add(CardInitializer.ThrowDebris(type));
            deck.add(CardInitializer.SkillsNotComplete(type));

            //Secondary Characters Cards
            addTrooperCards(deck, CharType.Trooper);
            return deck;
        }

        /// <summary>
        /// Populates Anakin Skywalker Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        public static List<Card> addAnakinCards(List<Card> deck)
        {
            CharType type = CharType.Anakin;
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_2(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Counterattack(type));
            deck.add(CardInitializer.Counterattack(type));
            deck.add(CardInitializer.Anger(type));
            deck.add(CardInitializer.Anger(type));
            deck.add(CardInitializer.Calm(type));
            deck.add(CardInitializer.Calm(type));
            deck.add(CardInitializer.WrathAnakin(type));
            deck.add(CardInitializer.WrathAnakin(type));
            deck.add(CardInitializer.WrathAnakin(type));

            //Secondary Characters Cards
            addPadmeCards(deck, CharType.Padme);
            return deck;
        }

        /// <summary>
        /// Populates Luke Skywalker Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        public static List<Card> addLukeCards(List<Card> deck)
        {
            CharType type = CharType.Luke;
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_2(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Justice(type));
            deck.add(CardInitializer.Justice(type));
            deck.add(CardInitializer.ChildrenOfTheForce(type));
            deck.add(CardInitializer.ChildrenOfTheForce(type));
            deck.add(CardInitializer.ChildrenOfTheForce(type));
            deck.add(CardInitializer.IWillNotFightYou(type));
            deck.add(CardInitializer.IWillNotFightYou(type));
            deck.add(CardInitializer.IWillNotFightYou(type));

            //Secondary Characters Cards
            addLeiaCards(deck, CharType.Leia);
            return deck;
        }

        /// <summary>
        /// Populates Han Solo Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        public static List<Card> addHanCards(List<Card> deck)
        {
            CharType type = CharType.Han;
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_2(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_2_2(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.GamblersLuck(type));
            deck.add(CardInitializer.GamblersLuck(type));
            deck.add(CardInitializer.GamblersLuck(type));
            deck.add(CardInitializer.HeroicRetreat(type));
            deck.add(CardInitializer.HeroicRetreat(type));
            deck.add(CardInitializer.NeverTellMeTheOdds(type));
            deck.add(CardInitializer.NeverTellMeTheOdds(type));
            addChewieCards(deck, CharType.Chewie);
            return deck;
        }

        /// <summary>
        /// Populates Obi-Wan Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        public static List<Card> addObiWanCards(List<Card> deck)
        {
            CharType type = CharType.Obi;
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_3_3(type));
            deck.add(CardInitializer.Card_3_3(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.ForceControl(type));
            deck.add(CardInitializer.ForceControl(type));
            deck.add(CardInitializer.JediAttack(type));
            deck.add(CardInitializer.JediAttack(type));
            deck.add(CardInitializer.JediAttack(type));
            deck.add(CardInitializer.JediBlock(type));
            deck.add(CardInitializer.JediBlock(type));
            deck.add(CardInitializer.JediBlock(type));
            deck.add(CardInitializer.JediMindTrick(type));
            deck.add(CardInitializer.ForceQuickness(type));
            deck.add(CardInitializer.ForceQuickness(type));
            deck.add(CardInitializer.ForceBalance(type));

            //Secondary Characters Cards
            addTrooperCards(deck, CharType.Trooper);
            return deck;
        }

        /// <summary>
        /// Populates Yoda Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        public static List<Card> addYodaCards(List<Card> deck)
        {
            CharType type = CharType.Yoda;
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_3_3(type));
            deck.add(CardInitializer.Card_3_3(type));
            deck.add(CardInitializer.Card_2_4(type));
            deck.add(CardInitializer.Card_2_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Card_1_5(type));
            deck.add(CardInitializer.ForceStrike(type));
            deck.add(CardInitializer.ForceStrike(type));
            deck.add(CardInitializer.Serenity(type));
            deck.add(CardInitializer.Serenity(type));
            deck.add(CardInitializer.ForceRebound(type));
            deck.add(CardInitializer.Insight(type));
            deck.add(CardInitializer.Insight(type));
            deck.add(CardInitializer.ForceLift(type));
            deck.add(CardInitializer.ForceLift(type));
            deck.add(CardInitializer.ForceLift(type));
            deck.add(CardInitializer.ForcePush(type, 3));
            deck.add(CardInitializer.ForcePush(type, 3));

            //Secondary Characters Cards
            addTrooperCards(deck, CharType.Trooper);
            return deck;
        }

        /// <summary>
        /// Populates Darth Maul Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        public static List<Card> addMaulCards(List<Card> deck)
        {
            CharType type = CharType.Maul;
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_2(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.AthleticSurge(type));
            deck.add(CardInitializer.AthleticSurge(type));
            deck.add(CardInitializer.AthleticSurge(type));
            deck.add(CardInitializer.SithSpeed(type));
            deck.add(CardInitializer.SithSpeed(type));
            deck.add(CardInitializer.SithSpeed(type));
            deck.add(CardInitializer.SuperSithSpeed(type));
            deck.add(CardInitializer.SuperSithSpeed(type));
            deck.add(CardInitializer.SuperSithSpeed(type));
            deck.add(CardInitializer.MartialDefense(type));
            deck.add(CardInitializer.BlindingSurge(type));
            deck.add(CardInitializer.BlindingSurge(type));

            //secondary character CardInitializers
            addTrooperCards(deck, CharType.BattleDroid);
            return deck;
        }

        /// <summary>
        /// Populates Boba Fett Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        public static List<Card> addBobaCards(List<Card> deck)
        {
            CharType type = CharType.Boba;
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_2(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_2_2(type));
            deck.add(CardInitializer.Card_2_2(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.WristCable(type));
            deck.add(CardInitializer.WristCable(type));
            deck.add(CardInitializer.ThermalDetonator(type));
            deck.add(CardInitializer.ThermalDetonator(type));
            deck.add(CardInitializer.RocketRetreat(type));
            deck.add(CardInitializer.RocketRetreat(type));
            deck.add(CardInitializer.RocketRetreat(type));
            deck.add(CardInitializer.KyberDart(type));
            deck.add(CardInitializer.DeadlyAim(type));

            //secondary character CardInitializers
            addGreedoCards(deck, CharType.Greedo);
            return deck;
        }

        /// <summary>
        /// Populates Jango Fett Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        public static List<Card> addJangoCards(List<Card> deck)
        {
            CharType type = CharType.Jango;
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_2(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_2_2(type));
            deck.add(CardInitializer.Card_2_2(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.WristCable(type));
            deck.add(CardInitializer.WristCable(type));
            deck.add(CardInitializer.FlameThrower(type));
            deck.add(CardInitializer.FireUpTheJetPack(type));
            deck.add(CardInitializer.MissileLaunch(type));
            deck.add(CardInitializer.RocketRetreat(type));
            deck.add(CardInitializer.RocketRetreat(type));
            deck.add(CardInitializer.RocketRetreat(type));

            //secondary character CardInitializers
            addZamCards(deck, CharType.Zam);
            return deck;
        }

        /// <summary>
        /// Populates Emperor Palpatine Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        public static List<Card> addEmperorCards(List<Card> deck)
        {
            CharType type = CharType.Emperor;
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_4_2(type));
            deck.add(CardInitializer.Card_3_3(type));
            deck.add(CardInitializer.Card_3_3(type));
            deck.add(CardInitializer.Card_2_4(type));
            deck.add(CardInitializer.Card_2_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Card_1_5(type));
            deck.add(CardInitializer.ForceLightning(type));
            deck.add(CardInitializer.ForceLightning(type));
            deck.add(CardInitializer.ForceLightning(type));
            deck.add(CardInitializer.ForceLightning(type));
            deck.add(CardInitializer.Meditation(type));
            deck.add(CardInitializer.Meditation(type));
            deck.add(CardInitializer.FutureForeseen(type));
            deck.add(CardInitializer.FutureForeseen(type));
            deck.add(CardInitializer.LetGoOfYourHatred(type));
            deck.add(CardInitializer.LetGoOfYourHatred(type));
            deck.add(CardInitializer.RoyalCommand(type));
            deck.add(CardInitializer.YouWillDie(type));
            //secondary character CardInitializers
            addCrimsonGuardCards(deck, CharType.CrimsonGuard);
            return deck;
        }

        /*
         * Secondary Character Cards
         */
        /// <summary>
        /// Populates Padme Amidala Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        private static List<Card> addPadmeCards(List<Card> deck, CharType type)
        {
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_2(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.PreciseShot(type));
            deck.add(CardInitializer.ShotOnTheRun(type));
            deck.add(CardInitializer.Protection(type));
            return deck;
        }

        /// <summary>
        /// Populates Leia Skywalker Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        private static List<Card> addLeiaCards(List<Card> deck, CharType type)
        {
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_2(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.LatentForceAbilities(type));
            deck.add(CardInitializer.LatentForceAbilities(type));
            deck.add(CardInitializer.LukesInTrouble(type));
            deck.add(CardInitializer.LukesInTrouble(type));
            return deck;
        }

        /// <summary>
        /// Populates Chewbacca Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        private static List<Card> addChewieCards(List<Card> deck, CharType type)
        {
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_2(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Bowcaster(type));
            deck.add(CardInitializer.WookieHealing(type));
            deck.add(CardInitializer.WookieInstincts(type));
            deck.add(CardInitializer.ItsNotWise(type));
            deck.add(CardInitializer.ItsNotWise(type));
            return deck;
        }

        /// <summary>
        /// Populates Zam Wesell Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        private static List<Card> addZamCards(List<Card> deck, CharType type)
        {
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_2(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Assassination(type));
            deck.add(CardInitializer.SniperShot(type));
            deck.add(CardInitializer.SniperShot(type));
            deck.add(CardInitializer.SniperShot(type));
            return deck;
        }

        /// <summary>
        /// Populates Greedo Deck
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        private static List<Card> addGreedoCards(List<Card> deck, CharType type)
        {
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_2(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.DesperateShot(type));
            deck.add(CardInitializer.SuddenArrival(type));
            deck.add(CardInitializer.SuddenArrival(type));
            return deck;
        }

        /// <summary>
        /// Populates List with Trooper Cards
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        private static List<Card> addTrooperCards(List<Card> deck, CharType type)
        {
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_2_1(type));
            deck.add(CardInitializer.Card_2_1(type));
            deck.add(CardInitializer.Card_2_1(type));
            deck.add(CardInitializer.Card_1_2(type));
            deck.add(CardInitializer.Card_1_2(type));
            deck.add(CardInitializer.Card_1_2(type));
            deck.add(CardInitializer.Card_1_2(type));
            return deck;
        }

        /// <summary>
        /// Populate List with Super Battle Droid Cards
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        private static List<Card> addSuperBattleDroidCards(List<Card> deck, CharType type)
        {
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_2(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_1_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            return deck;
        }

        /// <summary>
        /// Populate List with Crimson Guard Cards
        /// </summary>
        /// <param name="deck">List</param>
        /// <returns>List</returns>
        private static List<Card> addCrimsonGuardCards(List<Card> deck, CharType type)
        {
            deck.add(CardInitializer.Card_5_1(type));
            deck.add(CardInitializer.Card_4_1(type));
            deck.add(CardInitializer.Card_3_2(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_3_1(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_2_3(type));
            deck.add(CardInitializer.Card_2_4(type));
            deck.add(CardInitializer.Card_1_4(type));
            return deck;
        }
}
