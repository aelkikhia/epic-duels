package initializers;
import objects.Square;
import objects.GameTypes.SquareState;
import objects.Board;

/**
 *
 * @author almutaz.m.elkikhia
 */
public class BoardInitializer
        {

        /// <summary>
        /// Initializes game board with empty squares
        /// </summary>
        /// <param name="board">Square[][]</param>
        /// <param name="x">int</param>
        /// <param name="y">int</param>
        public static void initialize(Square[][] board,int x,int y){

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    board[i][j] = new Square(SquareState.Empty, i, j);
                }
            }
        }
        
        public static void buildBoard(Board board){
	        switch(board.getType()){
	            case Geonosis:
	                board.setName("Geonosis Arena");
	                BoardInitializer.setGeonosis(board.getBoard());
	                break;
	            case Freezing_Chamber:
	                board.setName("Carbon-Freezing Chamber");
	                BoardInitializer.setFreezingChamber(board.getBoard());
	                break;
	            case Throne_Room:
	                board.setName("Emperor's Throne Room");
	                BoardInitializer.setThroneRoom(board.getBoard());
	                break;
	            case Kamino:
	                board.setName("Kamino Platform");
	                BoardInitializer.setKamino(board.getBoard());
	                break;
	            default:
	                break;
	        }
        }

        /// <summary>
        /// Set Geonosis Arena Board
        /// </summary>
        /// <param name="board">Square[][]</param>
        /// <param name="team1">ArrayList</param>
        /// <param name="team2">ArrayList</param>
        public static void setGeonosis(Square[][] board)
        {
            //fallen pillar obstruction
            board[0][6].setState(SquareState.Obstacle);
            board[1][6].setState(SquareState.Obstacle);
            board[2][6].setState(SquareState.Obstacle);
            board[3][6].setState(SquareState.Obstacle);

            board[6][6].setState(SquareState.Obstacle);
            board[7][6].setState(SquareState.Obstacle);
            board[8][6].setState(SquareState.Obstacle);
            board[9][6].setState(SquareState.Obstacle);
            board[9][5].setState(SquareState.Obstacle);

            board[5][1].setState(SquareState.Obstacle);
            board[6][1].setState(SquareState.Obstacle);
            board[7][1].setState(SquareState.Obstacle);
            board[7][2].setState(SquareState.Obstacle);
        }

        /// <summary>
        /// Set Emperor's Throne Room Board
        /// </summary>
        /// <param name="board">Square[][]</param>
        /// <param name="team1">ArrayList</param>
        /// <param name="team2">ArrayList</param>
        public static void setThroneRoom(Square[][] board)
        {
            board[0][0].setState(SquareState.Obstacle);
            board[1][0].setState(SquareState.Obstacle);
            board[2][0].setState(SquareState.Obstacle);
            board[3][0].setState(SquareState.Obstacle);
            board[4][0].setState(SquareState.Obstacle);
            board[5][0].setState(SquareState.Obstacle);
            board[0][1].setState(SquareState.Obstacle);
            board[1][1].setState(SquareState.Obstacle);

            board[0][6].setState(SquareState.Obstacle);
            board[1][6].setState(SquareState.Obstacle);
            board[2][6].setState(SquareState.Obstacle);
            board[3][6].setState(SquareState.Obstacle);
            board[4][6].setState(SquareState.Obstacle);
            board[5][6].setState(SquareState.Obstacle);
            board[0][5].setState(SquareState.Obstacle);
            board[1][5].setState(SquareState.Obstacle);

            board[7][1].setState(SquareState.Hole);
            board[7][2].setState(SquareState.Hole);
            board[6][2].setState(SquareState.Hole);

            board[9][1].setState(SquareState.Hole);
            board[9][2].setState(SquareState.Hole);

            board[6][4].setState(SquareState.Hole);
            board[7][4].setState(SquareState.Hole);
            board[7][5].setState(SquareState.Hole);

            board[9][4].setState(SquareState.Hole);
            board[9][5].setState(SquareState.Hole);
        }

        /// <summary>
        /// Sets Carbon-Freezing Chamber Board
        /// </summary>
        /// <param name="board">Square[][]</param>
        /// <param name="team1">ArrayList</param>
        /// <param name="team2">ArrayList</param>
        public static void setFreezingChamber(Square[][] board)
        {
            board[0][0].setState(SquareState.Obstacle);
            board[1][0].setState(SquareState.Obstacle);
            board[2][0].setState(SquareState.Obstacle);
            board[0][1].setState(SquareState.Obstacle);
            board[1][1].setState(SquareState.Obstacle);

            board[0][6].setState(SquareState.Obstacle);
            board[1][6].setState(SquareState.Obstacle);
            board[2][6].setState(SquareState.Obstacle);
            board[0][5].setState(SquareState.Obstacle);
            board[1][5].setState(SquareState.Obstacle);

            board[8][0].setState(SquareState.Obstacle);
            board[9][0].setState(SquareState.Obstacle);

            board[8][6].setState(SquareState.Obstacle);
            board[9][6].setState(SquareState.Obstacle);
        }

        /// <summary>
        /// Set Kamino Platform Board
        /// </summary>
        /// <param name="board">Square[][]</param>
        /// <param name="team1">ArrayList</param>
        /// <param name="team2">ArrayList</param>
        public static void setKamino(Square[][] board)
        {
            board[6][0].setState(SquareState.Hole);
            board[7][0].setState(SquareState.Hole);
            board[8][0].setState(SquareState.Hole);
            board[9][0].setState(SquareState.Hole);
            board[6][1].setState(SquareState.Hole);
            board[7][1].setState(SquareState.Hole);
            board[8][1].setState(SquareState.Hole);

            board[6][6].setState(SquareState.Hole);
            board[7][6].setState(SquareState.Hole);
            board[8][6].setState(SquareState.Hole);
            board[9][6].setState(SquareState.Hole);
            board[6][5].setState(SquareState.Hole);
            board[7][5].setState(SquareState.Hole);
            board[8][5].setState(SquareState.Hole);

            board[3][3].setState(SquareState.Obstacle);
            board[1][4].setState(SquareState.Obstacle);
            board[2][4].setState(SquareState.Obstacle);
            board[3][4].setState(SquareState.Obstacle);
            board[2][5].setState(SquareState.Obstacle);
            board[3][5].setState(SquareState.Obstacle);
        }
    }
